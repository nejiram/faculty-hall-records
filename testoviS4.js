describe("Spirala 4 - Testovi", () => {
    
    //ZADATAK 4
    const db = require('./baza/db.js');
    var chai = require('chai');
    var assert = require('chai').assert;
    var expect = require('chai').expect;
    var should = require('chai').should;
    var mocha = require('mocha');
    var chaiHttp = require('chai-http');
    var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

    chai.use(chaiHttp);

    
    db.sequelize.sync({force:true}).then(() => {
        InicijalizirajBazu();
    });
    
    it('1. ruta /osoblje treba vratiti osoblje (ime i prezime) i sale(naziv)', () => {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = () => {
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                odgovor.should.have.property('osoblje').that.includes.all.keys('ime', 'prezime');
                odgovor.should.have.property('sale').that.includes.all.keys('naziv');
            }
        }
        ajax.open("GET", "/osoblje", true);
    });

    it('2. ruta /osoblje treba vracati imena: Neko, Drugi i Test', () => {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = () => {
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                let imeNeko = false;
                let imeDrugi = false;
                let imeTest = false;
                for (let i = 0; i < odgovor.length; i++){
                    if (odgovor[i].ime == "Neko"){
                        imeNeko = true;
                    }
                    else if (odgovor[i].ime == "Drugi"){
                        imeDrugi = true;
                    }
                    else if (odgovor[i].ime == "Test"){
                        imeTest = true;
                    }
                }
                assert.isTrue(imeNeko && imeDrugi && imeTest);
            }
        }
        ajax.open("GET", "/osoblje", true);
    });

    it('3. dohvatanje svih sala: trebaju biti dohvacene dvije sale: 1-11 i 1-15', () => {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = () => {
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                let sale = odgovor.sale;
                assert.isTrue(2 == sale.length);
            }
        }
        ajax.open("GET", "/osoblje", true);
    });

    it('4 .dohvatanje svih zauzeca: treba dohvatiti jedno vanredno i jedno redovno zauzece', () => {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                assert.isTrue((odgovor.periodicna.length == 1) && (odgovor.vanredna.length == 1));
            }
        }
        ajax.open("GET", "/ucitavanje", true);
        ajax.send();
    });

    it('5. azuriranje zauzeca: dodavanje nove vanredne rezervacije', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //trebaju biti dva vanredna
                assert.isTrue(odgovor.vanredna.length == 2);
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": false,
                "datum": "22.01.2020", 
                "pocetak": "08:00", 
                "kraj": "09:00", 
                "naziv": "1-11", 
                "predavac": "Neko Nekic"
            }
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));

    });

    it('6. azuriranje zauzeca: dodavanje nove periodicne rezervacije', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //trebaju biti dva periodicna
                assert.isTrue(odgovor.periodicna.length == 2);
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": true,
                "dan": "3",
                "semestar": "ljetni",
                "pocetak": "09:00",
                "kraj": "12:00",
                "naziv": "1-15",
                "predavac": "Test Test"
            };
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    });

    it('7. preklapanje zauzeca: preklapanje vanrednog zauzeca sa vanrednim', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //treba biti vracena greska - ne prazan string
                assert.isTrue(odgovor.greska != "");
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": false,
                "datum": "01.01.2020", 
                "pocetak": "12:00", 
                "kraj": "13:00", 
                "naziv": "1-11", 
                "predavac": "Neko Nekic"
            }
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    });

    it('8. preklapanje zauzeca: preklapanje periodicnog zauzeca sa periodicnim', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //treba biti vracena greska - ne prazan string
                assert.isTrue(odgovor.greska != "");
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": true,
                "dan": "0",
                "semestar": "zimski",
                "pocetak": "13:00",
                "kraj": "14:00",
                "naziv": "1-11",
                "predavac": "Test Test"
            };
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    });

    it('9. preklapanje zauzeca: preklapanje vanrednog zauzeca sa periodicnim', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //treba biti vracena greska - ne prazan string
                assert.isTrue(odgovor.greska != "");
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": false,
                "datum": "06.01.2020", 
                "pocetak": "13:00", 
                "kraj": "14:00", 
                "naziv": "1-11", 
                "predavac": "Neko Nekic"
            }
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    });

    it('10. preklapanje zauzeca: preklapanje periodicnog zauzeca sa vanrednim', () => {
        var ajax = new XMLHttpRequest();
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                //treba biti vracena greska - ne prazan string
                assert.isTrue(odgovor.greska != "");
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
            zauzece = {
                "periodicno": true,
                "dan": "3",
                "semestar": "zimski",
                "pocetak": "12:00",
                "kraj": "13:00",
                "naziv": "1-11",
                "predavac": "Test Test"
            };
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    });

    function InicijalizirajBazu(){
        let osobljeListaPromisea = [];
        let salaListaPromisea = [];
        let terminListaPromisea = [];
        let rezervacijaListaPromisea = [];
        return new Promise((resolve, reject) => {
            osobljeListaPromisea.push(db.Osoblje.create({
                ime: 'Neko', 
                prezime: 'Nekic',
                uloga: 'profesor'
            }));
            osobljeListaPromisea.push(db.Osoblje.create({
                ime: 'Drugi',
                prezime: 'Neko',
                uloga: 'asistent'
            }));
            osobljeListaPromisea.push(db.Osoblje.create({
                ime: 'Test',
                prezime: 'Test',
                uloga: 'asistent'
            }));
            Promise.all(osobljeListaPromisea).then((osoblje) => {
                let zaduzenaOsobaSala1 = osoblje.filter((o) => {return o.id === 1;})[0];
                let zaduzenaOsobaSala2 = osoblje.filter((o) => {return o.id === 2;})[0];
                salaListaPromisea.push(db.Sala.create({
                    naziv: '1-11'}).then((k) => {
                        k.setOsoblje(zaduzenaOsobaSala1);
                        return new Promise((resolve, reject) => { resolve(k); });
                    }));
                salaListaPromisea.push(db.Sala.create({
                    naziv: '1-15'}).then((k) => {
                        k.setOsoblje(zaduzenaOsobaSala2);
                        return new Promise((resolve, reject) => { resolve(k); });
                    }));
                Promise.all(salaListaPromisea).then((sala) => {
                    terminListaPromisea.push(db.Termin.create({
                        redovni: false,
                        dan: null,
                        datum: '01.01.2020',
                        semestar: null,
                        pocetak: '12:00',
                        kraj: '13:00'
                    }));
                    terminListaPromisea.push(db.Termin.create({
                        redovni: true,
                        dan: 0,
                        datum: null,
                        semestar: 'zimski',
                        pocetak: '13:00',
                        kraj: '14:00'
                    }));
                    Promise.all(terminListaPromisea).then((termin) => {
                        let termin1 = termin.filter((t) => {return t.id === 1})[0];
                        let termin2 = termin.filter((t) => {return t.id === 2})[0];
                        let rSala = sala.filter((s) => {return s.id === 1})[0];
                        let osoba1 = osoblje.filter((o) => {return o.id === 1})[0];
                        let osoba2 = osoblje.filter((o) => {return o.id === 3})[0];
                        rezervacijaListaPromisea.push(db.Rezervacija.create().then((k) => {
                            k.setTermin(termin1);
                            k.setSala(rSala);
                            k.setOsoblje(osoba1);
                            return new Promise((resolve, reject) => { resolve(k); });
                        }));
                        rezervacijaListaPromisea.push(db.Rezervacija.create().then((k) => {
                            k.setTermin(termin2);
                            k.setSala(rSala);
                            k.setOsoblje(osoba2);
                            return new Promise((resolve, reject) => { resolve(k); });
                        }));
                        Promise.all(rezervacijaListaPromisea).then((r) => {resolve(r); }).catch((err) => { console.log("Rezervacija greška: " + err)});
                    }).catch((err) => {
                        console.log("Termin greška: " + err);
                    });
                }).catch((err) => {
                    console.log("Sala greška: " + err);
                });
            }).catch((err) => {
                console.log("Osoblje greška: " + err);
            });
        });
    }
    

});