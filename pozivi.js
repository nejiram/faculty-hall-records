let Pozivi = (function(){
    
    //privatni atributi
    var ajax = new XMLHttpRequest();
    var nizSlika = [];
    var prvaUcitana = 0;
    var posljednjaUcitana = 2;
    var ucitaneSveSlike = false;
    var brojSlika = 0;

    //funkcije

    //zadatak 1
    function ucitavanjePodatakaImpl(){
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                console.log(odgovor);
                Kalendar.ucitajPodatke(odgovor.periodicna, odgovor.vanredna);
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
        }
        ajax.open("GET", "/ucitavanje", true);
        ajax.send();
    }

    //zadatak 2
    function zauzimanjeSaleImpl(dan, datum, mjesec, semestar, sala, pocetak, kraj, periodicno, predavac){
        //slanje zahtjeva na server
        let zauzece;
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){            
                let odgovor = JSON.parse(ajax.responseText);
                if (odgovor.greska == ""){
                    Kalendar.ucitajPodatke(odgovor.periodicna, odgovor.vanredna);
                } else {
                    alert(odgovor.greska);
                }
                
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
        }
        if (periodicno) {
            zauzece = {
                "periodicno": periodicno,
                "dan": dan,
                "semestar": semestar,
                "pocetak": pocetak,
                "kraj": kraj,
                "naziv": sala,
                "predavac": predavac
            };
        }
        else {
            zauzece = {
                "periodicno": periodicno,
                "datum": datum, 
                "pocetak": pocetak, 
                "kraj": kraj, 
                "naziv": sala, 
                "predavac": predavac
            };
        }
        ajax.open("POST", "http://localhost:8080/zauzeca", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    }

    //zadatak 3 - ucitavanje slika prilikom ucitavanja stranice
    function ucitavanjeSlikaImpl(){
        let zahtjev = {ucitaneSveSlike: ucitaneSveSlike, prvaUcitana: prvaUcitana, posljednjaUcitana: posljednjaUcitana}
        if (nizSlika.length == 0){
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200){
                    let odgovor = JSON.parse(ajax.responseText);
                    if (odgovor.greska == ""){
                        nizSlika = odgovor.nizSlika;
                        ucitaneSveSlike = odgovor.ucitaneSveSlike;
                        brojSlika = odgovor.brojSlika;
                        if (!ucitaneSveSlike){
                            if (odgovor.nizSlika.length == 3){
                                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[0];
                                document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[1];
                                document.getElementById("slika3").src = 'http://localhost:8080/public/galerija/' + nizSlika[2];
                                
                                if (odgovor.nizSlika.length % 3 == 0){
                                    prvaUcitana = posljednjaUcitana + 1;
                                    posljednjaUcitana = posljednjaUcitana + 3;
                                    if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                                }
                                else if (odgovor.nizSlika.length % 3 == 1){
                                    prvaUcitana = posljednjaUcitana + 1;
                                    posljednjaUcitana = posljednjaUcitana + 3;
                                    if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                                }
                                else if (odgovor.nizSlika.length % 3 == 2){
                                    prvaUcitana = prvaUcitana + 3;
                                    posljednjaUcitana = posljednjaUcitana + 2;
                                    if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                                }
                                if (posljednjaUcitana > brojSlika){
                                    ucitaneSveSlike = true;
                                    document.getElementById("sljedece").disabled = true;
                                }
                                if (prvaUcitana >= 3){
                                    document.getElementById("prethodne").disabled  = false;
                                }
                            }
                            else if (odgovor.nizSlika.length == 2){
                                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[0];
                                document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[1];
                                document.getElementById("slika3").style.visibility = "hidden";
                                prvaUcitana = posljednjaUcitana + 1;
                                posljednjaUcitana = posljednjaUcitana + 2;
                                if (posljednjaUcitana >= brojSlika){
                                    ucitaneSveSlike = true;
                                    document.getElementById("sljedece").disabled = true;
                                }
                            }
                            else if (odgovor.nizSlika.length == 1){
                                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[0];
                                document.getElementById("slika2").style.visibility = "hidden";
                                document.getElementById("slika3").style.visibility = "hidden";
                                prvaUcitana = posljednjaUcitana + 1;
                                posljednjaUcitana = posljednjaUcitana + 1;
                                if (posljednjaUcitana >= brojSlika){
                                    ucitaneSveSlike = true;
                                    document.getElementById("sljedece").disabled = true;
                                }
                            }
                            if (prvaUcitana < 4){
                                document.getElementById("prethodne").disabled  = true;
                            }
                        }
                    }
                    else {
                        document.getElementById("prethodne").disabled = true;
                        document.getElementById("sljedece").disabled = true;
                        alert(odgovor.greska);
                    }
                    zahtjev = {ucitaneSveSlike: ucitaneSveSlike, prvaUcitana: prvaUcitana, posljednjaUcitana: posljednjaUcitana};
                }
                else if (ajax.readyState == 4){
                    console.log(ajax.statusText + ": " + ajax.statusText);
                }
            }
            ajax.open("POST", 'http://localhost:8080/galerija', true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(zahtjev));
        }
    }

    //zadatak 3 - ucitavanje slika klikom na sljedece
    function ucitavanjeSljedecihSlikaImpl(){
        let zahtjev = {ucitaneSveSlike: ucitaneSveSlike, prvaUcitana: prvaUcitana, posljednjaUcitana: posljednjaUcitana};
        if (prvaUcitana >= 3){
            document.getElementById("prethodne").disabled  = false;
        }
        if (!ucitaneSveSlike){ //nisu ucitane sve slike sa servera - dobavljanje onih koje nisu ucitane
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200){
                    let odgovor = JSON.parse(ajax.responseText);
                    for (let i = 0; i < odgovor.nizSlika.length; i++){
                        nizSlika.push(odgovor.nizSlika[i]);
                    }
                    ucitaneSveSlike = odgovor.ucitaneSveSlike;
                    
                    if (odgovor.nizSlika.length == 3){
                        document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
                        document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 1];
                        document.getElementById("slika3").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 2];
                        
                        if (odgovor.nizSlika.length % 3 == 0){
                            prvaUcitana = posljednjaUcitana + 1;
                            posljednjaUcitana = posljednjaUcitana + 3;
                            if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                        }
                        else if (odgovor.nizSlika.length % 3 == 1){
                            prvaUcitana = posljednjaUcitana + 1;
                            posljednjaUcitana = posljednjaUcitana + 3;
                            if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                        }
                        else if (odgovor.nizSlika.length % 3 == 2){
                            prvaUcitana = prvaUcitana + 3;
                            posljednjaUcitana = posljednjaUcitana + 2;
                            if (posljednjaUcitana >= brojSlika) posljednjaUcitana = brojSlika;
                        }
                        if (posljednjaUcitana > brojSlika){
                            ucitaneSveSlike = true;
                            document.getElementById("sljedece").disabled = true;
                        }
                        
                    }
                    else if (odgovor.nizSlika.length == 2){
                        document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
                        document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 1];
                        document.getElementById("slika3").style.visibility = "hidden";
                        prvaUcitana = prvaUcitana + 3;
                        posljednjaUcitana = posljednjaUcitana + 2;
                        if (posljednjaUcitana >= brojSlika){
                            ucitaneSveSlike = true;
                            document.getElementById("sljedece").disabled = true;
                        }
                    }
                    else if (odgovor.nizSlika.length == 1){
                        document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
                        document.getElementById("slika2").style.visibility = "hidden";
                        document.getElementById("slika3").style.visibility = "hidden";
                        prvaUcitana = prvaUcitana + 3;
                        posljednjaUcitana = posljednjaUcitana + 1;
                        if (posljednjaUcitana >= brojSlika){
                            ucitaneSveSlike = true;
                            document.getElementById("sljedece").disabled = true;
                        }
                    }
                    zahtjev = {ucitaneSveSlike: ucitaneSveSlike, prvaUcitana: prvaUcitana, posljednjaUcitana: posljednjaUcitana};
                }
                else if (ajax.readyState == 4) {
                    console.log(ajax.status + ": " + ajax.statusText);
                }
            }
            ajax.open("POST", 'http://localhost:8080/galerija', true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(zahtjev));
        }
        else { //sve slike sa servera ucitane - nema pristupa serveru, dobavljamo ih lokalno (iz nizSlika)
            if (brojSlika - posljednjaUcitana > 3){
                prvaUcitana = posljednjaUcitana + 1;
                posljednjaUcitana = posljednjaUcitana + 3;
                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
                document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 1];
                document.getElementById("slika3").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 2];
                if (brojSlika - 1 <= posljednjaUcitana) {
                    document.getElementById("sljedece").disabled = true;
                }
            }
            else if (brojSlika - posljednjaUcitana == 3){
                prvaUcitana = posljednjaUcitana + 1;
                posljednjaUcitana = posljednjaUcitana + 2;
                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
                document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 1];
                document.getElementById("slika3").style.visibility = "hidden";
                if (brojSlika - 1 <= posljednjaUcitana) {
                    document.getElementById("sljedece").disabled = true;
                }
            }
            else if (brojSlika - posljednjaUcitana == 2){
                prvaUcitana = posljednjaUcitana + 1;
                posljednjaUcitana = posljednjaUcitana + 1;
                document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[posljednjaUcitana];
                document.getElementById("slika2").style.visibility = "hidden";
                document.getElementById("slika3").style.visibility = "hidden";
                
                if (brojSlika - 1 <= posljednjaUcitana) {
                    document.getElementById("sljedece").disabled = true;
                }
            }
        };
    }

    //zadatak 3 - ucitavanje slika klikom na prethodne
    function ucitavanjePrethodnihSlikaImpl(){
        if (prvaUcitana > 0){
            if (posljednjaUcitana > brojSlika){
                if (brojSlika % 3 == 0){
                    posljednjaUcitana = brojSlika - 1;
                    prvaUcitana == brojSlika - 3;
                }
                else if (brojSlika % 3 == 1){
                    posljednjaUcitana = brojSlika - 1;
                    prvaUcitana = brojSlika - 1;
                }
                else if (brojSlika % 3 == 2){
                    posljednjaUcitana = brojSlika - 1;
                    prvaUcitana = brojSlika - 2;
                }
            }
            if (prvaUcitana == posljednjaUcitana){
                prvaUcitana = prvaUcitana - 3;
                posljednjaUcitana = posljednjaUcitana - 1;
                document.getElementById("slika2").style.visibility = "visible";
                document.getElementById("slika3").style.visibility = "visible";
                if (prvaUcitana == 0) {
                    document.getElementById("prethodne").disabled  = true;
                    document.getElementById("sljedece").disabled = false;
                }
            }
            else if (posljednjaUcitana - prvaUcitana == 1){
                prvaUcitana = prvaUcitana - 3;
                posljednjaUcitana = posljednjaUcitana - 2;
                document.getElementById("slika3").style.visibility = "visible";
                if (prvaUcitana == 0) {
                    document.getElementById("prethodne").disabled  = true;
                    document.getElementById("sljedece").disabled = false;
                }
            }
            else {
                prvaUcitana = prvaUcitana - 3;
                posljednjaUcitana = posljednjaUcitana - 3;
                if (prvaUcitana == 0) {
                    document.getElementById("prethodne").disabled  = true;
                    document.getElementById("sljedece").disabled = false;
                }
            }
            document.getElementById("slika1").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana];
            document.getElementById("slika2").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 1];
            document.getElementById("slika3").src = 'http://localhost:8080/public/galerija/' + nizSlika[prvaUcitana + 2];
            if (prvaUcitana == 0) {
                document.getElementById("prethodne").disabled  = true;
                document.getElementById("sljedece").disabled = false;
            }
        }
    }

    //zadatak 4 - vraca broj posjeta stranici
    function brojPosjetaImpl(){
        ajax.onreadystatechange = function(){
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                document.getElementById("posjetiociChrome").innerHTML = odgovor.posjeteChrome;
                document.getElementById("posjetiociMozila").innerHTML = odgovor.posjeteFirefox;
                document.getElementById("posjetiociIP").innerHTML = odgovor.razliciteIPadrese.length;
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
        }
        ajax.open("POST", "http://localhost:8080/posjete", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send();
    }

    //spirala 4 
    //zadatak 1
    function vratiOsobljeISaleImpl(){
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                let osoblje = odgovor.osoblje;
                let sale = odgovor.sale;
                let selektOsoblje = document.getElementById("osobeSelektor");
                for (let i = 0; i < osoblje.length; i++){
                    let opcija = document.createElement("option");
                    opcija.text = osoblje[i]["ime"] + " " + osoblje[i]["prezime"];
                    selektOsoblje.add(opcija);
                }
                let selektSale = document.getElementById("saleSelektor");
                for (let i = 0; i < sale.length; i++){
                    let opcija = document.createElement("option");
                    opcija.text = sale[i]["naziv"];
                    selektSale.add(opcija);
                }
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
        }
        ajax.open("GET", "/osoblje", true);
        ajax.send();
    }

    //zadatak 3
    function vratiListuOsobljaImpl(){
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                let odgovor = JSON.parse(ajax.responseText);
                document.getElementById("osoblje").innerHTML = "";
                let osoblje = document.createElement("table");
                let osobljeBody =  document.createElement("tbody");
                
                let prviRed = document.createElement("tr");
                let redniBr = document.createElement("th");
                let redniBrText = document.createTextNode("");
                redniBr.appendChild(redniBrText);
                redniBr.style.visibility = "hidden";
                prviRed.appendChild(redniBr);
                let ime_i_prezime = document.createElement("th");
                let ime_i_prezimeText = document.createTextNode("Ime i prezime");
                ime_i_prezime.appendChild(ime_i_prezimeText);
                prviRed.appendChild(ime_i_prezime);
                let sala = document.createElement("th");
                let salaText = document.createTextNode("Sala");
                sala.appendChild(salaText);
                prviRed.appendChild(sala);
                osobljeBody.appendChild(prviRed);

                for (let i = 0; i < odgovor.length; i++){
                    let noviRed = document.createElement("tr");
                    redniBr = document.createElement("td");
                    ime_i_prezime = document.createElement("td");
                    sala = document.createElement("td");
                    ime_i_prezimeText = document.createTextNode(odgovor[i]["ime"] + " " + odgovor[i]["prezime"]);
                    salaText = document.createTextNode(odgovor[i]["sala"]);
                    redniBrText = document.createTextNode(i + 1);
                    ime_i_prezime.appendChild(ime_i_prezimeText);
                    sala.appendChild(salaText);
                    redniBr.appendChild(redniBrText);
                    redniBr.className = "redniBr";
                    noviRed.appendChild(redniBr);
                    noviRed.appendChild(ime_i_prezime);
                    noviRed.appendChild(sala);
                    osobljeBody.appendChild(noviRed);
                }
                osoblje.appendChild(osobljeBody);
                document.getElementById("osoblje").appendChild(osoblje);
            }
            else if (ajax.readyState == 4){
                console.log(ajax.status + ": " + ajax.statusText);
            }
        }
        ajax.open("GET", "/listaOsoblja", true);
        ajax.send();
    }

    return{
        ucitavanjePodataka: ucitavanjePodatakaImpl,
        zauzimanjeSale: zauzimanjeSaleImpl,
        ucitavanjeSlika: ucitavanjeSlikaImpl,
        ucitavanjeSljedecihSlika: ucitavanjeSljedecihSlikaImpl,
        ucitavanjePrethodnihSlika: ucitavanjePrethodnihSlikaImpl,
        brojPosjeta: brojPosjetaImpl,
        vratiOsobljeISale: vratiOsobljeISaleImpl,
        vratiListuOsoblja: vratiListuOsobljaImpl
    }
}());