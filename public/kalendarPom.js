var godina = 2020;
var trenutniMjesec = 0;

function bojenjeZauzeca(){
    var saleSelektor = document.getElementById("saleSelektor");
    var sala = saleSelektor.value;
    var pocetak = document.getElementsByTagName("input")[1].value;
    var kraj = document.getElementsByTagName("input")[2].value;
    let pocetakMin = parseInt(pocetak.substring(0,2)) * 60 + parseInt(pocetak.substring(3,5));
    let krajMin = parseInt(kraj.substring(0,2)) * 60 + parseInt(kraj.substring(3,5));
    if (krajMin <= pocetakMin) {
        alert("Vrijeme kraja ne može biti nakon vremena početka!");
    }
    else if (pocetak != "" || kraj != "" || sala != ""){
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, sala, pocetak, kraj);
    }
}
function iscrtavanjeKalendara(){;
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);
}
function iscrtavanjeSljedeceg(){
    trenutniMjesec++;
    Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);
    var saleSelektor = document.getElementById("saleSelektor");
    var sala = saleSelektor.value;
    var pocetak = document.getElementsByTagName("input")[1].value;
    var kraj = document.getElementsByTagName("input")[2].value;
    if (pocetak != "" && kraj != "" && sala != ""){
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, sala, pocetak, kraj);
    }
    if (trenutniMjesec == 11) document.getElementById("sljedeci").disabled  = true;
    else if (document.getElementById("prethodni").disabled  === true){
        document.getElementById("prethodni").disabled  = false;
    }
}
function iscrtavanjePrethodnog(){
    if (trenutniMjesec > 0){
        trenutniMjesec--;
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), trenutniMjesec);
        var saleSelektor = document.getElementById("saleSelektor");
        var sala = saleSelektor.value;
        var pocetak = document.getElementsByTagName("input")[1].value;
        var kraj = document.getElementsByTagName("input")[2].value;
        if (pocetak != "" && kraj != "" && sala != ""){
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), trenutniMjesec, sala, pocetak, kraj);
        }
        if (trenutniMjesec == 0) document.getElementById("prethodni").disabled  = true;
        else if (document.getElementById("sljedeci").disabled  === true){
            document.getElementById("sljedeci").disabled  = false;
        }
    } else {
        document.getElementById("prethodni").disabled  = true;
    }
    
}
setInterval(bojenjeZauzeca, 10);