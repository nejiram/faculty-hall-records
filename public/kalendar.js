let Kalendar = (function(){
    //
    //ovdje idu privatni atributi
    //
    var kalendar;
    var kalendarBody;
    var periodicnaZauzeca;
    var vanrednaZauzeca;

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){
        //implementacija ide ovdje
        var d = new Date();
        var godina = "2020";
        var brDana = 32 - new Date(godina, mjesec, 32).getDate();
        var prviDan = (new Date(godina, mjesec)).getDay();
        if (prviDan === 0) prviDan = 7; //jer inace stavi da je nedjelja 0
        var maxBrDana = false;
        var pocetakMin = vratiMinute(pocetak);
        var krajMin = vratiMinute(kraj);

        //postavi sve na zeleno
        for (let i = 1; i <= brDana + prviDan - 1; i++){
            for (let j = 0; j < 7; j++){
                if (parseInt(kalendar.rows[i].cells[j].innerHTML) === brDana){
                    kalendar.rows[i].cells[j].style.borderBottom = "7.5px solid #009A17";
                    if (kalendar.rows[i].cells[j].innerHTML == "") kalendar.rows[i].cells[j].style.visibility = "hidden";
                    maxBrDana = true;
                    break;
                }
                else if (parseInt(kalendar.rows[i].cells[j].innerHTML) < brDana){
                    kalendar.rows[i].cells[j].style.borderBottom = "7.5px solid #009A17";
                    if (kalendar.rows[i].cells[j].innerHTML == "") kalendar.rows[i].cells[j].style.visibility = "hidden";
                }
            }
            if (maxBrDana) break;
        }

        //oboji periodicna zauzeca
        if (periodicnaZauzeca.length > 0){
            var maxDan = false;
            for (let i = 0; i < periodicnaZauzeca.length; i++){
                var pPocetakMin = vratiMinute(periodicnaZauzeca[i]["pocetak"]);
                var pKrajMin = vratiMinute(periodicnaZauzeca[i]["kraj"]);
                if ((periodicnaZauzeca[i]["semestar"] === "ljetni") && (parseInt(mjesec) >= 1 && parseInt(mjesec) <= 5) && (sala === periodicnaZauzeca[i]["naziv"]) && ((pocetakMin > pPocetakMin && pocetakMin < pKrajMin) || (krajMin > pPocetakMin && krajMin < pKrajMin) || (pocetakMin <= pPocetakMin && krajMin >= pKrajMin))){
                    var dan = periodicnaZauzeca[i]["dan"];
                    var sedmica = kalendar.rows;
                    for (let j = 1; j < sedmica.length; j++){
                        maxDan = false;
                        for (let k = 0; k < 7; k++){
                            if (k === parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                                maxDan = true;
                                break;
                            }
                            else if (k != parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                maxDan = true;
                                break;
                            }
                            else if (k === parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) < brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                            }
                        }
                        if (maxDan) break;
                    }
                }
                if ((periodicnaZauzeca[i]["semestar"] === "zimski") && (parseInt(mjesec) === 0 || (parseInt(mjesec) >= 9 && parseInt(mjesec) <= 11)) && (sala === periodicnaZauzeca[i]["naziv"]) && ((pocetakMin > pPocetakMin && pocetakMin < pKrajMin) || (krajMin > pPocetakMin && krajMin < pKrajMin) || (pocetakMin <= pPocetakMin && krajMin >= pKrajMin))){
                    var dan = periodicnaZauzeca[i]["dan"];
                    var sedmica = kalendar.rows;
                    for (let j = 1; j < sedmica.length; j++){
                        maxDan = false;
                        for (let k = 0; k < 7; k++){
                            if (k === parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                                maxDan = true;
                                break;
                            }
                            else if (k != parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                maxDan = true;
                                break;
                            }
                            else if (k === parseInt(dan) && parseInt(sedmica[j].cells[k].innerHTML) < brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                            }
                        }
                        if (maxDan) break;
                    }
                }
            }
        }

        //oboji vanredna
        if (vanrednaZauzeca.length > 0){
            maxDan = false;
            for (let i = 0; i < vanrednaZauzeca.length; i++){
                var vPocetakMin = parseInt(vanrednaZauzeca[i]["pocetak"].substring(0,2)) * 60 + parseInt(vanrednaZauzeca[i]["pocetak"].substring(3,5));
                var vKrajMin = parseInt(vanrednaZauzeca[i]["kraj"].substring(0,2)) * 60 + parseInt(vanrednaZauzeca[i]["kraj"].substring(3,5));
                var vanredniMjesec = parseInt(vanrednaZauzeca[i]["datum"].substring(3,5)) - 1;
                if (vanredniMjesec === mjesec && sala === vanrednaZauzeca[i]["naziv"] && ((pocetakMin > vPocetakMin && pocetakMin < vKrajMin) || (krajMin > vPocetakMin && krajMin < vKrajMin) || (pocetakMin <= vPocetakMin && krajMin >= vKrajMin))){
                    var dan;
                    if (vanrednaZauzeca[i]["datum"].substring(0,1) === "0") dan = vanrednaZauzeca[i]["datum"].substring(1,2);
                    else dan = vanrednaZauzeca[i]["datum"].substring(0,2);
                    var sedmica = kalendar.rows;
                    for (let j = 1; j <sedmica.length; j++){
                        maxDan = false;
                        for (let k = 0; k < 7; k++){
                            if (sedmica[j].cells[k].innerHTML === dan && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                                maxDan = true;
                                break;
                            }
                            else if (sedmica[j].cells[k].innerHTML != dan && parseInt(sedmica[j].cells[k].innerHTML) === brDana){
                                maxDan = true;
                                break;
                            }
                            else if (sedmica[j].cells[k].innerHTML === dan && parseInt(sedmica[j].cells[k].innerHTML) < brDana){
                                sedmica[j].cells[k].style.borderBottom = "7.5px solid #D50032";
                            }
                        }
                    }
                }
            }
        }

        //dodavanje onclick
        maxBrDana = false;
        for (let i = 1; i <= brDana + prviDan - 1; i++){
            for (let j = 0; j < 7; j++){
                if (parseInt(kalendar.rows[i].cells[j].innerHTML) === brDana){
                    if (kalendar.rows[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && kalendar.rows[i].cells[j].innerHTML != ""){
                        kalendar.rows[i].cells[j].onclick = function(){
                            let r = potvrdaZauzeca(kalendar.rows[i].cells[j].innerHTML);
                            if (r == true){
                                let datum = kalendar.rows[i].cells[j].innerHTML;
                                if (parseInt(datum) >= 1 && parseInt(datum) <= 9){
                                    datum = "0" + datum;
                                }
                                zauzmi(datum);
                            }
                        }
                    }
                    else if (kalendar.rows[i].cells[j].style.borderBottom == "7.5px solid rgb(213, 0, 50)" && kalendar.rows[i].cells[j].innerHTML != ""){
                        kalendar.rows[i].cells[j].onclick = function(){
                            greskaZauzetaSala(kalendar.rows[i].cells[j].innerHTML);
                        }
                    }
                    maxBrDana = true;
                    break;
                }
                else if (parseInt(kalendar.rows[i].cells[j].innerHTML) < brDana){
                    if (kalendar.rows[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && kalendar.rows[i].cells[j].innerHTML != ""){
                        kalendar.rows[i].cells[j].onclick = function(){
                            let r = potvrdaZauzeca(kalendar.rows[i].cells[j].innerHTML);
                            if (r == true){
                                let datum = kalendar.rows[i].cells[j].innerHTML;
                                if (parseInt(datum) >= 1 && parseInt(datum) <= 9){
                                    datum = "0"+datum;
                                }
                                zauzmi(datum);
                            }
                        }
                    }
                    else if (kalendar.rows[i].cells[j].style.borderBottom == "7.5px solid rgb(213, 0, 50)" && kalendar.rows[i].cells[j].innerHTML != ""){
                        kalendar.rows[i].cells[j].onclick = function(){
                            greskaZauzetaSala(kalendar.rows[i].cells[j].innerHTML);
                        }
                    }
                }
            }
            if (maxBrDana) break;
        }
    }
    function ucitajPodatkeImpl(periodicna, vanredna){
        //implementacija ide ovdje
        
        periodicnaZauzeca = periodicna;
        vanrednaZauzeca = vanredna;
        
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec){
    
        document.getElementById("kalendar").innerHTML = "";
        kalendar = document.createElement("table");
        kalendarBody = document.createElement("tbody");

        //prikazuje naziv mjeseca iznad tabele
        var mjeseciNiz = ['1.', '2.', '3.', '4.', '5.', '6.', '7.', '8.', '9.', '10.', '11.', '12.'];
        var caption = document.createElement("thead");
        var captionText = document.createTextNode(mjeseciNiz[mjesec]);
        caption.appendChild(captionText);
        kalendarBody.appendChild(caption);

        //kreiramo prvi red tabele u kojem su dani u sedmici
        var sedmica = document.createElement("tr");
        var dan, danNaziv;
        var niz_dani = ['PON', 'UTO', 'SRI', 'ČET', 'PET', 'SUB', 'NED'];
        for (let i = 0; i < 7; i++){
            dan = document.createElement("th");
            danNaziv = document.createTextNode(niz_dani[i]);
            dan.appendChild(danNaziv);
            sedmica.appendChild(dan);
            kalendarBody.appendChild(sedmica);
        }

        //uzimamo neophodne podatke za kreiranje kalendara
        var godina = "2020";
        var brDana = 32 - new Date(godina, mjesec, 32).getDate();
        var prviDan = (new Date(godina, mjesec)).getDay();
        if (prviDan === 0) prviDan = 7; //jer inace stavi da je nedjelja 0

        //kreiramo ostale redove tabele u kojima su datumi      
        var datum, datumText;
        sedmica = document.createElement("tr");
        for (let i = 1; i <= brDana + prviDan - 1; i++){
            for (let j = 0; j < 7; j++){
                datum = document.createElement("td");
                if (i < prviDan || i > brDana + prviDan - 1){
                    datumText = document.createTextNode("");
                }
                else{
                    datumText = document.createTextNode(i - prviDan + 1);
                }
                datum.appendChild(datumText);
            }
            sedmica.appendChild(datum);
            kalendarBody.appendChild(sedmica);
            if (i % 7 === 0) sedmica = document.createElement("tr");
        }
        kalendar.appendChild(kalendarBody);
        kalendarRef.appendChild(kalendar);
    }
    return {
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    iscrtajKalendar: iscrtajKalendarImpl
    }
    }());