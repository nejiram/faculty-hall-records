let assert = chai.assert;
describe('Kalendar', function(){
    describe('obojiZauzeca()', function(){
        it ('1. kada podaci nisu ucitani ne treba obojiti niti jedan dan', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([],[]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = false;
            //ako je barem jedan crven, nisu obojeni ok
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        break;
                    }
                }
                if (maxDan || crvena) break;
            }
            assert.equal(crvena, false);
        });
        it ('2. treba da se oboji dan i kada postoje duple vrijednosti za zauzece istog termina', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([{"dan": "2", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "2", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],
                                    [{"datum": "30.11.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"datum": "30.11.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = true;
            //treba biti obojena svaka srijeda u sedmici + 30.11.
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        crvena = false;
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (j === 2 && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(0, 154, 23)"){
                        crvena = false;
                        break;
                    }
                }
                if (maxDan || !crvena) break;
            }
            assert.equal(crvena, true);
        })
        it ('3. kada postoji periodicno zauzece za drugi semestar, ne treba se obojiti zauzece', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([{"dan": "2", "semestar": "ljetni", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],[]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = false;
            //novembar citav treba biti zelen -> ako je bar jedan crven nisu obojeni ok
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        break;
                    }
                }
                if (maxDan || crvena) break;
            }
            assert.equal(crvena, false);
        });
        it ('4. kada postoji zauzece u drugom mjesecu, ne treba se obojiti zauzece', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([],[{"datum": "06.12.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = false;
            //novembar citav treba biti zelen -> ako je bar jedan crven nisu obojeni ok
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena = true;
                        break;
                    }
                }
                if (maxDan || crvena) break;
            }
            assert.equal(crvena, false);
        }); 
        it ('5. kada svi termini u mjesecu zauzeti, svi dani trebaju biti obojeni', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([{"dan": "0", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "1", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "2", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "3", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "4", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "5", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"},
                                    {"dan": "6", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],[]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let zelena = false;
            //ako je makar jedan zelen, nisu obojeni ok
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(0, 154, 23)"){
                        zelena = true;
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(0, 154, 23)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(0, 154, 23)"){
                        zelena = true;
                        break;
                    }
                }
                if (maxDan || zelena) break;
            }
            assert.equal(zelena, false);
        });
        it ('6. dva puta uzastopni poziv obojiZauzeca() - boja zauzeca ostaje ista', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([{"dan": "1", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],
                                [{"datum": "30.11.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let crvena = {"sedmica": "", "dan": ""};
            let nizCrvenih = [];
            let maxDan = false;
            //prvi poziv - pamtimo koje celije su obojene
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                    }
                }
                if(maxDan) break;
            }  
            //drugi poziv - provjeravamo da li su obojene iste celije kao u drugom pozivu
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            sedmica = document.getElementsByTagName("table")[0].rows;
            maxDan = false;
            let obojeneIspravno = true;
            for (let i = 1; i <sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    for (let k = 0; k < nizCrvenih.length; k++){
                        if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                                obojeneIspravno = false;
                                maxDan = true;
                                break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                            maxDan = true;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                            obojeneIspravno = false;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                            maxDan = true;
                            break;
                        }
                    }
                    if (maxDan || !obojeneIspravno) break;
                }
                if (maxDan || !obojeneIspravno) break;
            }
            assert.equal(obojeneIspravno, true);
        });
        it ('7. ucitajPodatke(), obojiZauzeca(), ucitajPodatke() - drugi podaci, obojiZauzeca() - primjenjuju se samo posljednji ucitani podaci', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let crvena = {"sedmica": "", "dan": ""};
            let nizCrvenih = [];
            let maxDan = false;
            //prvi poziv - pamtimo koje celije su obojene
            Kalendar.ucitajPodatke([{"dan": "1", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],
                                [{"datum": "30.11.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                    }
                }
                if(maxDan) break;
            }
            //drugi poziv - mijenjamo podatke, provjeravamo da li su idalje obojeni oni iz prvog poziva - ako nisu, test je OK
            maxDan = false;
            let obojeneIspravno = true;
            Kalendar.ucitajPodatke([{"dan": "3", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],
                                [{"datum": "13.11.2019", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            sedmica = document.getElementsByTagName("table")[0].rows;
            for (let i = 1; i <sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    for (let k = 0; k < nizCrvenih.length; k++){
                        if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                                obojeneIspravno = false;
                                maxDan = true;
                                break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                            maxDan = true;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                            obojeneIspravno = false;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                            maxDan = true;
                            break;
                        }
                    }
                    if (maxDan || !obojeneIspravno) break;
                }
                if (maxDan || !obojeneIspravno) break;
            }
            assert.equal(obojeneIspravno, true);
        });
        it ('8. periodicno zauzece za zimski semestar treba da oboji sve odgovarajuce dane svih mjeseci iz tog semestra', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([{"dan": "1", "semestar": "zimski", "pocetak": "10:30", "kraj": "12:00", "naziv": "va1", "predavac": "Profesor"}],[]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "va1", "10:30", "12:00");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = {"sedmica": "", "dan": ""};
            let nizCrvenih = [];
            //pamtimo koji dani su bili obojeni u novembru
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        maxDan = true;
                        break;
                    }
                    else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom === "7.5px solid rgb(213, 0, 50)"){
                        crvena["sedmica"] = i;
                        crvena["dan"] = j;
                        nizCrvenih.push(crvena);
                    }
                }
                if(maxDan) break;
            }
            //provjeravamo da li su isti dani obojeni u januaru
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 0, "va1", "10:30", "12:00");
            sedmica = document.getElementsByTagName("table")[0].rows;
            maxDan = false;
            let obojeneIspravnoJan = true;
            for (let i = 1; i <sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    for (let k = 0; k < nizCrvenih.length; k++){
                        if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                                obojeneIspravnoJan = false;
                                maxDan = true;
                                break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                            maxDan = true;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                            obojeneIspravnoJan = false;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                            maxDan = true;
                            break;
                        }
                    }
                    if (maxDan || !obojeneIspravnoJan) break;
                }
                if (maxDan || !obojeneIspravnoJan) break;
            }
            //provjeravamo da li su isti dani obojeni u oktobru
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 9, "va1", "10:30", "12:00");
            sedmica = document.getElementsByTagName("table")[0].rows;
            maxDan = false;
            let obojeneIspravnoOkt = true;
            for (let i = 1; i <sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    for (let k = 0; k < nizCrvenih.length; k++){
                        if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                                obojeneIspravnoOkt = false;
                                maxDan = true;
                                break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                            maxDan = true;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                            obojeneIspravnoOkt = false;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                            maxDan = true;
                            break;
                        }
                    }
                    if (maxDan || !obojeneIspravnoOkt) break;
                }
                if (maxDan || !obojeneIspravnoOkt) break;
            }
            //provjeravamo da li su isti dani obojeni u decembtu
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 11, "va1", "10:30", "12:00");
            sedmica = document.getElementsByTagName("table")[0].rows;
            maxDan = false;
            let obojeneIspravnoDec = true;
            for (let i = 1; i <sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    for (let k = 0; k < nizCrvenih.length; k++){
                        if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                                obojeneIspravnoDec = false;
                                maxDan = true;
                                break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                            maxDan = true;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) < brDana && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)" && i === parseInt(nizCrvenih[k]["sedmica"]) && j === parseInt(nizCrvenih[k]["dan"])){
                            obojeneIspravnoDec = false;
                            break;
                        }
                        else if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                            maxDan = true;
                            break;
                        }
                    }
                    if (maxDan || !obojeneIspravnoDec) break;
                }
                if (maxDan || !obojeneIspravnoDec) break;
            }
            assert.equal(obojeneIspravnoJan && obojeneIspravnoOkt && obojeneIspravnoDec, true);
        });
        it ('9. redovno zauzece treba da oboji 5.11., 13.11., 21.11. i 29.11.', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            Kalendar.ucitajPodatke([],[{"datum": "05.11.2019", "pocetak": "17:00", "kraj": "18:30", "naziv": "ma", "predavac": "Profesor"},
                                    {"datum": "13.11.2019", "pocetak": "17:00", "kraj": "18:30", "naziv": "ma", "predavac": "Profesor"},
                                    {"datum": "21.11.2019", "pocetak": "17:00", "kraj": "18:30", "naziv": "ma", "predavac": "Profesor"},
                                    {"datum": "29.11.2019", "pocetak": "17:00", "kraj": "18:30", "naziv": "ma", "predavac": "Profesor"}]);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"), 10, "ma", "17:00", "18:30");
            let brDana = 32 - new Date(2019, 10, 32).getDate();
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let maxDan = false;
            let crvena = true;
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){if (parseInt(sedmica[i].cells[j].innerHTML) === brDana){
                        maxDan = true;
                        break;
                    }
                    else if ((parseInt(sedmica[i].cells[j].innerHTML) === "5" || parseInt(sedmica[i].cells[j].innerHTML) === "13" || parseInt(sedmica[i].cells[j].innerHTML) === "21" || parseInt(sedmica[i].cells[j].innerHTML) === "29") && sedmica[i].cells[j].style.borderBottom != "7.5px solid rgb(213, 0, 50)"){
                        crvena = false;
                        break;
                    }
                }
                if (maxDan || !crvena) break;
            }
            assert.equal(crvena, true);
        })
    });
    describe('iscrtajKalendar()', function(){
        it ('1. treba prikazati 30 dana za mjesec od 30 dana, npr. novembar', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let brDana = String(32 - new Date(2019, 10, 32).getDate());
            let brCelija = 0;
            let maxDan = false;
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (sedmica[i].cells[j].innerHTML === brDana){
                        brCelija++;
                        maxDan = true;
                        break;
                    }
                    else if (sedmica[i].cells[j].innerHTML != "") brCelija++;
                }
                if (maxDan) break;
            }
            assert.equal(brCelija, 30);
        });
        it ('2. treba prikazati 31 dan za mjesec od 31 dan, npr. oktobar', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let brDana = String(32 - new Date(2019, 9, 32).getDate());
            let brCelija = 0;
            let maxDan = false;
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (sedmica[i].cells[j].innerHTML === brDana){
                        brCelija++;
                        maxDan = true;
                        break;
                    }
                    else if (sedmica[i].cells[j].innerHTML != "") brCelija++;
                }
                if (maxDan) break;
            }
            assert.equal(brCelija, 31);
        });
        it ('3. prvi dan u trenutnom mjesecu treba biti u petak', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let prviDan;
            for (let i = 0; i < 7; i++){
                if (sedmica[1].cells[i].innerHTML === "1"){
                    prviDan = i;
                    break;
                }
            }
            assert.equal(prviDan, 4);
        });
        it ('4. posljednji dan u trenutnom mjesecu treba biti u subotu', function(){
            let kalendar = Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let posljednjiDan;
            for (let i = 0; i < 7; i++){
                if (sedmica[sedmica.length - 1].cells[i].innerHTML === "30"){
                    posljednjiDan = i;
                    break;
                }
            }
            assert.equal(posljednjiDan, 5);
        });
        it ('5. poziv za januar - brojevi od 1 do 31, 1. je u utorak', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let prviDan;
            let posljednjiDan;
            //je li prvi dan u utorak
            for (let i = 0; i < 7; i++){
                if (sedmica[1].cells[i].innerHTML === "1"){
                    prviDan = i;
                }
                if (sedmica[sedmica.length - 1].cells[i].innerHTML === "31"){
                    posljednjiDan = i;
                    break;
                }
            }
            assert.equal(prviDan,1);
            assert.equal(posljednjiDan, 3);
        });
        it ('6. treba prikazati broj mjeseca iznad kalendara - npr. 5. za maj', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 4);
            let mjesec = document.getElementsByTagName("table")[0].getElementsByTagName("thead")[0].innerHTML;
            assert.equal(mjesec, "5.");
        });
        it ('7. 22.12.2019. treba biti u nedjelju', function(){
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);
            let sedmica = document.getElementsByTagName("table")[0].rows;
            let dan;
            let danNadjen = false;
            for (let i = 1; i < sedmica.length; i++){
                for (let j = 0; j < 7; j++){
                    if (sedmica[i].cells[j].innerHTML === "22"){
                        dan = j;
                        danNadjen = true;
                        break;
                    }
                }
                if (danNadjen) break;
            }
            assert.equal(dan, 6);
        });
    });
});