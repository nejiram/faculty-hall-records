//model Sala
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const Sala = sequelize.define('Sala', {
        naziv: Sequelize.STRING,
        zaduzenaOsoba: {
            unique: false,
            type: Sequelize.INTEGER
        }
    });
    return Sala;
}