const Sequelize = require('sequelize');
const sequelize = new Sequelize("DBWT19", "root", "root", {
    host: "127.0.0.1", 
    dialect: "mysql", 
    logging: false
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.Osoblje = sequelize.import(__dirname + '/osoblje.js');
db.Rezervacija = sequelize.import(__dirname + '/rezervacija.js');
db.Termin = sequelize.import(__dirname + '/termin.js');
db.Sala = sequelize.import(__dirname + '/sala.js');

//relacije
//za kreiranje 1:1 koriste se hasOne i belongsTo zajedno
//za kreiranje 1:n koriste se hasMany i belongsTo zajedno
//za kreiranje m:n koriste se dvije belongsToMany zajedno

//Osoblje - Rezervacija 1:n - jedna osoba može imati više rezervacija
db.Osoblje.hasMany(db.Rezervacija, {
    foreignKey: 'osoba'
});
db.Rezervacija.belongsTo(db.Osoblje, {
    foreignKey: 'osoba'
});

//Rezervacija - Termin 1:1 - jedan termin može imati samo jednu rezervaciju
db.Termin.hasOne(db.Rezervacija, {
    foreignKey: 'termin'
});
db.Rezervacija.belongsTo(db.Termin, {
    foreignKey: 'termin'
});

//Sala-Rezervacija 1:n - jedna sala može imati više rezervacija
db.Sala.hasMany(db.Rezervacija, {
    foreignKey: 'sala'
});
db.Rezervacija.belongsTo(db.Sala, {
    foreignKey: 'sala'
});

//Sala-Osoblje 1:1 - jedna osoba može biti u jednoj sali
db.Osoblje.hasOne(db.Sala, {
    foreignKey: 'zaduzenaOsoba',
});
db.Sala.belongsTo(db.Osoblje, {
    foreignKey: 'zaduzenaOsoba'
});

module.exports = db;