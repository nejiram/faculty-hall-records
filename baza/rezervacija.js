//model Rezervacija
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const Rezervacija = sequelize.define('Rezervacija', {
        termin: {
            unique: true,
            type: Sequelize.INTEGER
        },
        sala: {
            unique: false,
            type: Sequelize.INTEGER  
        },
        osoba: {
            unique: false,
            type: Sequelize.INTEGER
        }
    });
    return Rezervacija;
}