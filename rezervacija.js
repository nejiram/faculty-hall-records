var sala = "";
var predavac = "";
var pocetak = "";
var kraj = "";
var periodicno = "";

//zadatak 1
function ucitavanjePodataka(){
    Pozivi.ucitavanjePodataka();
}

//zadatak 2
//funkcija koja javlja gresku ako je sala zauzeta
function greskaZauzetaSala(datum){
    ucitajSaForme();
    let mjesec = document.getElementsByTagName("table")[0].getElementsByTagName("thead")[0].innerHTML;
    mjesec = mjesec.substr(0, mjesec.length-1);
    let semestar = vratiSemestar(mjesec);
    let dan = vratiDan(datum, mjesec);
    datum = datum + "." + mjesec + "." + "2020";
    if (periodicno){
        var dani = ['ponedjeljak', 'utorak', 'srijedu', 'četvrtak', 'petak', 'subotu', 'nedjelju'];
        alert("Greška: Sala " + sala + " je već rezervisana u " + dani[dan] + " za " + semestar +" semestar" + ", u terminu od " + pocetak + " do " + kraj + "!");
    }
    else {
        alert("Greška: Sala " + sala + " je već rezervisana " + datum + " u terminu od " + pocetak + " do " + kraj + "!");
    }
}

//funkcija za potvrdu zauzeca
function potvrdaZauzeca(datum){
    ucitajSaForme();
    let mjesec = document.getElementsByTagName("table")[0].getElementsByTagName("thead")[0].innerHTML;
    mjesec = mjesec.substr(0, mjesec.length-1);
    let semestar = vratiSemestar(mjesec);
    let dan = vratiDan(datum, mjesec);
    datum = datum + "." + mjesec + "." + "2020";
    let r;
    if (sala == "" || predavac == "" || pocetak == "" || kraj == ""){
        alert("Greška: nisu uneseni svi podaci!");
        return false;
    }
    else {
        if (periodicno){
            var dani = ['svakog ponedjeljka', 'svakog utorka', 'svake srijede', 'svakog četvrtka', 'svakog petka', 'svake subote', 'svake nedjelje'];
            if (semestar == "ljetni") semestar = "ljetnom";
            else if (semestar == "zimski")  semestar = "zimskom";
            r = confirm("Da li ste sigurni da želite rezervisati salu " + sala + " " + dani[dan] + " u " + semestar + " semestru, u terminu od " + pocetak + " do " + kraj + "?");
        }
        else {
            r = confirm("Da li ste sigurni da želite rezervisati salu " + sala + " " + datum + " u terminu od " + pocetak + " do " + kraj + "?");
        }
        return r;
    }
}

//funkcija za zauzimanje sale ako nije crvena
function zauzmi(datum){
    ucitajSaForme();
    if (pocetak != "" || kraj != "" || sala != "") {
        let mjesec = document.getElementsByTagName("table")[0].getElementsByTagName("thead")[0].innerHTML;
        mjesec = mjesec.substr(0, mjesec.length-1);
        let semestar = vratiSemestar(mjesec);
        let dan = vratiDan(datum, mjesec);
        if (parseInt(mjesec) >= 1 && parseInt(mjesec) <= 9){
            mjesec = "0" + mjesec;
        }
        datum = datum + "." + mjesec + "." + "2020";
        if (semestar == "" && periodicno) {
            alert("Periodično zauzeće može biti samo u toku ljetnog ili u toku zimskog semestra!");
        }
        else{
            Pozivi.zauzimanjeSale(dan, datum, mjesec, semestar, sala, pocetak, kraj, periodicno, predavac);
        }
    }
    else {
        alert("Podaci o rezervaciji moraju biti uneseni!");
    }
}

//funkcija koja ucitava podatke sa forme
function ucitajSaForme(){
    var saleSelektor = document.getElementById("saleSelektor");
    var predavacSelektor = document.getElementById("osobeSelektor");
    predavac = predavacSelektor.value;
    sala = saleSelektor.value;
    pocetak = document.getElementsByTagName("input")[1].value;
    kraj = document.getElementsByTagName("input")[2].value;
    if (document.getElementById("periodicna").checked){
        periodicno = true;
    }
    else{
        periodicno = false;
    }
}

//funkcija koja vraca redni broj dana u sedmici
function vratiDan(datum, mjesec){
    let prviDani = [2,5,6,2,4,0,2,5,1,3,6,1];
    let dan = ((parseInt(datum) + parseInt(prviDani[mjesec-1])) % 7) - 1;
    if (dan == -1) dan = 6;
    return dan;
}

//funkcija koja vraca semestar kao string
function vratiSemestar(mjesec){
    let semestar = "";
    if (mjesec == 1 || mjesec == 10 || mjesec == 11 || mjesec == 12){
        semestar = "zimski";
    }
    else if (mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5 || mjesec == 6){
        semestar = "ljetni";
    }
    return semestar;
}

//funkcija koja pretvara sate u minute
function vratiMinute(vrijeme){
    let minute = parseInt(vrijeme.substring(0,2)) * 60 + parseInt(vrijeme.substring(3,5));
    return minute;
}

//vracanje osoba iz baze u selekt
function popuniSelect(){
    Pozivi.vratiOsobljeISale();
}