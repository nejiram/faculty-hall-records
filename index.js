const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const url = require('url');
const useragent = require('express-useragent');

//baza
const Sequelize = require('sequelize');
const db = require('./baza/db.js');
const {Op} = require('sequelize');

db.sequelize.sync({force:true}).then(() => {
    InicijalizirajBazu().then(() => {
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
    });
});

//spirala 3
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/json'));
app.use('/galerija', express.static(__dirname + '/public/galerija'));
app.use(useragent.express());

app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/pocetna.html');
});

app.get('/sale.html', function(req, res){
    res.sendFile(__dirname + '/sale.html');
});

app.get('/unos.html', function(req, res){
    res.sendFile(__dirname + '/unos.html');
});

app.get('/rezervacija.html', function(req, res){
    res.sendFile(__dirname + '/rezervacija.html');
});

app.get('/pozivi.js', function(req, res){
    res.sendFile(__dirname + '/pozivi.js');
});

app.get('/json/zauzeca.json', function(req, res){
    res.sendFile(__dirname + '/json/zauzeca.json')
});

app.get('/kalendar.js', function(req, res){
    res.sendFile(__dirname + '/kalendar.js');
});

app.get('/kalendarPom.js', function(req, res){
    res.sendFile(__dirname + '/kalendarPom.js');
});

app.get('/rezervacija.js', function(req, res){
    res.sendFile(__dirname + '/rezervacija.js');
});

app.get('/pocetna.js', function(req, res){
    res.sendFile(__dirname + "/pocetna.js")
});

app.post('/galerija', function(req, res){
    let prva = req.body.prvaUcitana;
    let treca = req.body.posljednjaUcitana;
    let ucitaneSveSlike = req.body.ucitaneSveSlike;
    let granica = treca;
    let nizSlika = [];
    let greska = "";
    fs.readdir(__dirname + '/public/galerija/', function(err, files){
        if (err) throw err;
        let brojSlika = files.length;
        if (parseInt(brojSlika) == 0){
            greska = "Greška: Galerija je prazna!";

        }
        else {
            if (treca >= brojSlika) {
                treca = brojSlika;
                granica = treca - 1;
            }
            if (treca <= brojSlika){
                for (let i = prva; i <= granica; i++){
                    if (treca <= brojSlika) {
                        nizSlika.push(files[i]);
                        app.get('/public/galerija/' + files[i], function(req, res){
                            res.sendFile(__dirname + '/public/galerija/' + files[i]);
                        });
                    }
                }
            }
        }
        let odgovor = {nizSlika: nizSlika, ucitaneSveSlike: ucitaneSveSlike, brojSlika: brojSlika, greska: greska};
        res.json(odgovor);
    });
});

app.get('/json/broj_posjetilaca.json', function(req, res){
    res.sendFile(__dirname + '/json/broj_posjetilaca.json');
});

app.post('/posjete', function(req, res){
    fs.readFile('./json/broj_posjetilaca.json', 'utf-8', (err, data) => {
        let novePosjete = "";
        
        let noveChrome = JSON.parse(data).posjeteChrome;
        let noveFirefox = JSON.parse(data).posjeteFirefox;
        let noveIPadrese = JSON.parse(data).razliciteIPadrese;

        let nova = true;
        if (toString(req.connection.remoteAddress) != "::1"){
            for (let i = 0; i < noveIPadrese.length; i++){
                if (toString(noveIPadrese[i]) == toString(req.connection.remoteAddress)){
                    nova = false;
                    break;
                }
            }
        }
        if (nova){
            noveIPadrese.push(req.connection.remoteAddress);
        }
        
        if (req.useragent.isChrome) {
            noveChrome++;
        }
        else if (req.useragent.isFirefox){
            noveFirefox++;
        }
        novePosjete = {posjeteChrome: noveChrome, posjeteFirefox: noveFirefox, razliciteIPadrese: noveIPadrese};
        novePosjete = JSON.stringify(novePosjete);
        
        
        fs.writeFile('./json/broj_posjetilaca.json', novePosjete, 'utf-8', (err) => {
            if (err) throw err; 
            res.end(novePosjete);
        });
    });
});

//spirala 4
//pomocna funkcija za inicijalizaciju baze
function InicijalizirajBazu(){
    let osobljeListaPromisea = [];
    let salaListaPromisea = [];
    let terminListaPromisea = [];
    let rezervacijaListaPromisea = [];
    return new Promise((resolve, reject) => {
        osobljeListaPromisea.push(db.Osoblje.create({
            ime: 'Neko', 
            prezime: 'Nekic',
            uloga: 'profesor'
        }));
        osobljeListaPromisea.push(db.Osoblje.create({
            ime: 'Drugi',
            prezime: 'Neko',
            uloga: 'asistent'
        }));
        osobljeListaPromisea.push(db.Osoblje.create({
            ime: 'Test',
            prezime: 'Test',
            uloga: 'asistent'
        }));
        Promise.all(osobljeListaPromisea).then((osoblje) => {
            let zaduzenaOsobaSala1 = osoblje.filter((o) => {return o.id === 1;})[0];
            let zaduzenaOsobaSala2 = osoblje.filter((o) => {return o.id === 2;})[0];
            salaListaPromisea.push(db.Sala.create({
                naziv: '1-11'}).then((k) => {
                    k.setOsoblje(zaduzenaOsobaSala1);
                    return new Promise((resolve, reject) => { resolve(k); });
                }));
            salaListaPromisea.push(db.Sala.create({
                naziv: '1-15'}).then((k) => {
                    k.setOsoblje(zaduzenaOsobaSala2);
                    return new Promise((resolve, reject) => { resolve(k); });
                }));
            Promise.all(salaListaPromisea).then((sala) => {
                terminListaPromisea.push(db.Termin.create({
                    redovni: false,
                    dan: null,
                    datum: '01.01.2020',
                    semestar: null,
                    pocetak: '12:00',
                    kraj: '13:00'
                }));
                terminListaPromisea.push(db.Termin.create({
                    redovni: true,
                    dan: 0,
                    datum: null,
                    semestar: 'zimski',
                    pocetak: '13:00',
                    kraj: '14:00'
                }));
                Promise.all(terminListaPromisea).then((termin) => {
                    let termin1 = termin.filter((t) => {return t.id === 1})[0];
                    let termin2 = termin.filter((t) => {return t.id === 2})[0];
                    let rSala = sala.filter((s) => {return s.id === 1})[0];
                    let osoba1 = osoblje.filter((o) => {return o.id === 1})[0];
                    let osoba2 = osoblje.filter((o) => {return o.id === 3})[0];
                    rezervacijaListaPromisea.push(db.Rezervacija.create().then((k) => {
                        k.setTermin(termin1);
                        k.setSala(rSala);
                        k.setOsoblje(osoba1);
                        return new Promise((resolve, reject) => { resolve(k); });
                    }));
                    rezervacijaListaPromisea.push(db.Rezervacija.create().then((k) => {
                        k.setTermin(termin2);
                        k.setSala(rSala);
                        k.setOsoblje(osoba2);
                        return new Promise((resolve, reject) => { resolve(k); });
                    }));
                    Promise.all(rezervacijaListaPromisea).then((r) => {resolve(r); }).catch((err) => { console.log("Rezervacija greška: " + err)});
                }).catch((err) => {
                    console.log("Termin greška: " + err);
                });
            }).catch((err) => {
                console.log("Sala greška: " + err);
            });
        }).catch((err) => {
            console.log("Osoblje greška: " + err);
        });
    });
}

//zadatak 1
app.get('/osoblje', function(req, res){
    db.Osoblje.findAll().then((o) => {
        db.Sala.findAll().then((s) => {
            let osoblje = []; 
            let sale = [];      
            for (let i = 0; i <o.length; i++){
                osoblje.push({ime: o[i].ime, prezime: o[i].prezime});
            }
            for (let i = 0; i < s.length; i++){
                sale.push({naziv: s[i].naziv});
            }
            res.send({osoblje: osoblje, sale: sale});
        }).catch((err) => {console.log("Greška pri dobavljanju sala: " + err);});
    }).catch((err) => {console.log("Greška pri dobavljanju osoblja: " + err);});
})

//zadatak 2
//ucitavanje podataka iz baze
app.get('/ucitavanje', function(req, res) {
    let periodicna = [];
    let vanredna = [];
    let periodicno;
    let dan;
    let datum = "";
    let semestar = "";
    let pocetak = ""
    let kraj = "";
    let naziv = "";
    let predavac = "";
    db.Rezervacija.findAll().then((r) => {
        db.Osoblje.findAll().then((o) => {
            db.Termin.findAll().then((t) => {
                db.Sala.findAll().then((s) => {
                    for (let i = 0; i < r.length; i++){for (let j = 0; j < o.length; j++){
                            if (r[i].osoba == o[j].id){
                                predavac = o[j].ime + " " + o[j].prezime;
                                break;
                            }
                        }
                        for (let j = 0; j < t.length; j++){
                            if (r[i].termin == t[j].id){
                                periodicno = t[j].redovni;
                                dan = t[j].dan;
                                datum = t[j].datum;
                                semestar = t[j].semestar;
                                pocetak = t[j].pocetak;
                                kraj = t[j].kraj;
                                break;
                            }
                        }
                        for (let j = 0; j < s.length; j++){
                            if (r[i].sala == s[j].id){
                                naziv = s[j].naziv;
                                break;
                            }
                        }
                        if (periodicno){
                            periodicna.push({dan: dan, semestar: semestar, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: predavac});
                        }
                        else {
                            vanredna.push({datum: datum, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: predavac});
                        }
                    }
                    res.send({periodicna: periodicna, vanredna: vanredna});
                }).catch((err) => {console.log("Greška pri dobavljanju sala: " + err);});
            }).catch((err) => {console.log("Greška pri dobavljanju termina " + err);});
        }).catch((err) => {console.log("Greška pri dobavljanju osoblja " + err);});
    }).catch((err) => {console.log("Greška pri dobavljanju rezervacija: " + err);});
});

//upis podataka u bazu
app.post('/zauzeca', function(req, res){
    let tijelo = req.body;
    let novoZauzece;
    let periodicno = false;
    if (tijelo['periodicno']){
        periodicno = true;
        novoZauzece = {
            "dan": tijelo['dan'],
            "semestar": tijelo['semestar'],
            "pocetak": tijelo['pocetak'],
            "kraj": tijelo['kraj'],
            "naziv": tijelo['naziv'],
            "predavac": tijelo['predavac']
        };
    }
    else {
        novoZauzece = {
            "datum": tijelo['datum'],
            "pocetak": tijelo['pocetak'], 
            "kraj": tijelo['kraj'], 
            "naziv": tijelo['naziv'], 
            "predavac": tijelo['predavac']
        };
    }
    let noviPocetak = parseInt(novoZauzece["pocetak"].substring(0,2)) * 60 + parseInt(novoZauzece["pocetak"].substring(3,5));
    let noviKraj = parseInt(novoZauzece["kraj"].substring(0,2)) * 60 + parseInt(novoZauzece["kraj"].substring(3,5));
    db.Rezervacija.findAll().then((r) => {
    let periodicna = [];
    let vanredna = [];
    let periodicnoStara;
    let dan;
    let datum = "";
    let semestar = "";
    let pocetak = ""
    let kraj = "";
    let naziv = "";
    let predavac = "";
    db.Rezervacija.findAll().then((r) => {
        db.Osoblje.findAll().then((o) => {
            db.Termin.findAll().then((t) => {
                db.Sala.findAll().then((s) => {
                    //ucitavamo periodicna i vanredna
                    for (let i = 0; i < r.length; i++){
                        for (let j = 0; j < o.length; j++){
                            if (r[i].osoba == o[j].id){
                                predavac = o[j].ime + " " + o[j].prezime;
                                break;
                            }
                        }
                        for (let j = 0; j < t.length; j++){
                            if (r[i].termin == t[j].id){
                                periodicnoStara = t[j].redovni;
                                dan = t[j].dan;
                                datum = t[j].datum;
                                semestar = t[j].semestar;
                                pocetak = t[j].pocetak;
                                kraj = t[j].kraj;
                                break;
                            }
                        }
                        for (let j = 0; j < s.length; j++){
                            if (r[i].sala == s[j].id){
                                naziv = s[j].naziv;
                                break;
                            }
                        }
                        if (periodicnoStara){
                            periodicna.push({dan: dan, semestar: semestar, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: predavac});
                        }
                        else {
                            vanredna.push({datum: datum, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: predavac});
                        }
                    }
                    //validacija ulaza
                    let imaIstoPzauzeceJeP = false;
                    let imaIstoVzauzeceJeP = false;
                    let imaIstoPzauzeceJeV = false;
                    let imaIstoVzauzeceJeV = false;
                    if (periodicno){
                        //novo zauzece je periodicno
                        //provjeravamo ima li isto periodicno zauzece
                        for (let i = 0; i < periodicna.length; i++){
                            let pocetak = parseInt(periodicna[i].pocetak.substring(0,2)) * 60 + parseInt(periodicna[i].pocetak.substring(3,5));
                            let kraj = parseInt(periodicna[i].kraj.substring(0,2)) * 60 + parseInt(periodicna[i].kraj.substring(3,5));
                            if ((periodicna[i].dan == novoZauzece["dan"]) && (periodicna[i].semestar == novoZauzece["semestar"]) && (periodicna[i].naziv == novoZauzece["naziv"]) && ((noviPocetak > pocetak && noviPocetak < kraj) || (noviKraj > kraj && noviKraj < kraj) || (noviPocetak <= pocetak && noviKraj >= kraj))){
                                imaIstoPzauzeceJeP = true;
                                break;
                            }
                        }
                        //provjeravamo ima li vanredno zauzece koje se poklapa
                        for (let i = 0; i < vanredna.length; i++){
                            let pocetak = parseInt(vanredna[i].pocetak.substring(0,2)) * 60 + parseInt(vanredna[i].pocetak.substring(3,5));
                            let kraj = parseInt(vanredna[i].kraj.substring(0,2)) * 60 + parseInt(vanredna[i].kraj.substring(3,5));
                            //na osnovu datuma racunamo dan i semestar
                            let datum = vanredna[i].datum.split(".");
                            let prviDani = [2,5,6,2,4,0,2,5,1,3,6,1];
                            let mjesec = datum[1];
                            let dan = ((parseInt(datum[0]) + parseInt(prviDani[mjesec-1])) % 7) - 1;
                            if (dan == -1) dan = 6;
                            let semestar = "";
                            mjesec = parseInt(mjesec);
                            if (mjesec == 1 || mjesec == 10 || mjesec == 11 || mjesec == 12){
                                semestar = "zimski";
                            }
                            else if (mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5 || mjesec == 6){
                                semestar = "ljetni";
                            }
                            if ((parseInt(dan) == parseInt(novoZauzece["dan"])) && (semestar == novoZauzece["semestar"]) && (vanredna[i].naziv == novoZauzece["naziv"]) && ((noviPocetak > pocetak && noviPocetak < kraj) || (noviKraj > kraj && noviKraj < kraj) || (noviPocetak <= pocetak && noviKraj >= kraj))){
                                imaIstoVzauzeceJeP = true;
                                break;
                            }
                        }
                    } else {
                        //novo zauzece je vanredno
                        //provjeravamo ima li isto zauzece u vanrednim
                        for (let i = 0; i < vanredna.length; i++){
                            let pocetak = parseInt(vanredna[i].pocetak.substring(0,2)) * 60 + parseInt(vanredna[i].pocetak.substring(3,5));
                            let kraj = parseInt(vanredna[i].kraj.substring(0,2)) * 60 + parseInt(vanredna[i].kraj.substring(3,5));
                            if ((vanredna[i].datum == novoZauzece["datum"]) && (vanredna[i].naziv == novoZauzece["naziv"]) && ((noviPocetak > pocetak && noviPocetak < kraj) || (noviKraj > kraj && noviKraj < kraj) || (noviPocetak <= pocetak && noviKraj >= kraj))){
                                imaIstoVzauzeceJeV = true;
                                break;
                            }
                        }
                        //provjeravamo poklapa li se novo zauzece sa nekim periodicnim
                        for (let i = 0; i < periodicna.length; i++){
                            let pocetak = parseInt(periodicna[i].pocetak.substring(0,2)) * 60 + parseInt(periodicna[i].pocetak.substring(3,5));
                            let kraj = parseInt(periodicna[i].kraj.substring(0,2)) * 60 + parseInt(periodicna[i].kraj.substring(3,5));
                            //na osnovu datuma racunamo dan i semestar
                            let datum = novoZauzece["datum"].split(".");
                            let prviDani = [2,5,6,2,4,0,2,5,1,3,6,1];
                            let mjesec = datum[1];
                            let dan = ((parseInt(datum[0]) + parseInt(prviDani[mjesec-1])) % 7) - 1;
                            if (dan == -1) dan = 6;
                            let semestar = "";
                            mjesec = parseInt(mjesec);
                            if (mjesec == 1 || mjesec == 10 || mjesec == 11 || mjesec == 12){
                                semestar = "zimski";
                            }
                            else if (mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5 || mjesec == 6){
                                semestar = "ljetni";
                            }
                            if ((parseInt(dan) == parseInt(periodicna[i].dan)) && (semestar == periodicna[i].semestar) && (vanredna[i].naziv == novoZauzece["naziv"]) && ((noviPocetak > pocetak && noviPocetak < kraj) || (noviKraj > kraj && noviKraj < kraj) || (noviPocetak <= pocetak && noviKraj >= kraj))){
                                imaIstoPzauzeceJeV = true;
                                break;
                            }
                        }
                    }
                    //provjeravamo da li je moguca rezervacija
                    //ako nije ispisujemo gresku
                    let greska = "";
                    let trebaRezervisati = false;
                    if (imaIstoPzauzeceJeP == true || imaIstoPzauzeceJeV == true || imaIstoVzauzeceJeP == true || imaIstoVzauzeceJeV == true){
                        if (periodicno) {
                            var dani = ['ponedjeljak', 'utorak', 'srijedu', 'četvrtak', 'petak', 'subotu', 'nedjelju'];
                            greska = "Nije moguće rezervisati salu " + novoZauzece["naziv"] + " za " + dani[novoZauzece["dan"]] + " u terminu od " + novoZauzece["pocetak"] + " do " + novoZauzece["kraj"] + "!";
                        } else {
                            greska = "Nije moguće rezervisati salu " + novoZauzece["naziv"] + " za navedeni datum " + novoZauzece["datum"] + " u terminu od " + novoZauzece["pocetak"] + " do " + novoZauzece["kraj"] + "!";
                        }
                        res.end(JSON.stringify({periodicna: periodicna, vanredna: vanredna, greska: greska}));
                    } else {
                        trebaRezervisati = true;
                    }
                    //ako je moguce, rezervisemo salu
                    if (trebaRezervisati){
                        //ako je periodicno
                        let ime_i_prezime = novoZauzece["predavac"].split(" ");
                        let ime = ime_i_prezime[0];
                        let prezime = ime_i_prezime[1];
                        if (periodicno){
                            db.Termin.create({
                                redovni: true,
                                dan: novoZauzece["dan"],
                                datum: null,
                                semestar: novoZauzece["semestar"],
                                pocetak: novoZauzece["pocetak"],
                                kraj: novoZauzece["kraj"]
                            }).then((noviTermin) => {
                                db.Sala.findOne({where: {naziv: novoZauzece["naziv"]}}).then((novaSala) => {
                                    db.Osoblje.findOne({where: {
                                        ime: ime,
                                        prezime: prezime
                                    }}).then((novaOsoba) => {
                                        db.Rezervacija.create({
                                            termin: noviTermin.id,
                                            sala: novaSala.id,
                                            osoba: novaOsoba.id
                                        }).then((novaRezervacija) => {
                                            periodicna.push(novoZauzece);
                                            res.end(JSON.stringify({periodicna: periodicna, vanredna: vanredna, greska: greska}));
                                        }).catch((err) => {console.log("Greška pri dodavanju nove rezervacije: " + err);});
                                    }).catch((err) => {console.log("Greška pri dodavanju nove osobe: " + err);});
                                }).catch((err) => {console.log("Greška pri upisu nove sale: " + err);});
                            }).catch((err) => {console.log("Greška pri upisu novog termina: " + err);});
                        } else {
                            db.Termin.create({
                                redovni: false,
                                dan: null,
                                datum: novoZauzece["datum"],
                                semestar: novoZauzece["semestar"],
                                pocetak: novoZauzece["pocetak"],
                                kraj: novoZauzece["kraj"]
                            }).then((noviTermin) => {
                                db.Sala.findOne({where: {
                                    naziv: novoZauzece["naziv"]}}).then((novaSala) => {
                                    db.Osoblje.findOne({where: {
                                        ime: ime,
                                        prezime: prezime
                                    }}).then((novaOsoba) => {
                                        db.Rezervacija.create({
                                            termin: noviTermin.id,
                                            sala: novaSala.id,
                                            osoba: novaOsoba.id
                                        }).then((novaRezervacija) => {
                                            vanredna.push(novoZauzece);
                                            res.end(JSON.stringify({periodicna: periodicna, vanredna: vanredna, greska: greska}));
                                        }).catch((err) => {console.log("Greška pri dodavanju nove rezervacije: " + err);});
                                    }).catch((err) => {console.log("Greška pri dodavanju nove osobe: " + err);});
                                }).catch((err) => {console.log("Greška pri upisu nove sale: " + err);});
                            }).catch((err) => {console.log("Greška pri upisu novog termina: " + err);});
                        }
                    }
                }).catch((err) => {console.log("Greška pri dobavljanju sala: " + err);});
            }).catch((err) => {console.log("Greška pri dobavljanju termina " + err);});
        }).catch((err) => {console.log("Greška pri dobavljanju osoblja " + err);});
    }).catch((err) => {console.log("Greška pri dobavljanju rezervacija: " + err);});
    }).catch((err) => {console.log("Greška pri dobavljanju rezervacija: " + err);});
});

//zadatak 3
app.get('/listaOsoblja', function(req, res){
    let listaOsoblja = [];
    //uzimamo trenutni datum 
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();
    let datum = dd + '.' + mm + '.' + yyyy;
    //uzimamo trenutno vrijeme 
    let hh = today.getHours(); 
    let min = today.getMinutes();
    let vrijeme = parseInt(hh) * 60 + parseInt(min);
    
    //racunamo redni broj dana u sedmici na osnovu datum 
    let prviDani = [2,5,6,2,4,0,2,5,1,3,6,1];
    let dan = ((parseInt(dd) + parseInt(prviDani[mm-1])) % 7) - 1;
    if (dan == -1) dan = 6;
    db.Osoblje.findAll().then((o) => {
        db.Rezervacija.findAll().then((r) => {
            db.Termin.findAll().then((t) => {
                db.Sala.findAll().then((s) => {
                    let imaTermin = false;
                    let sala = "u kancelariji";
                    for (let i = 0; i < o.length; i++){
                        imaRezervacija = false;
                        sala = "u kancelariji";
                        pocetak = "09:00";
                        kraj = "17:00";
                        for (let j = 0; j < r.length; j++){
                            imaTermin = false;
                            if (o[i].id == r[j].osoba){
                                for (let k = 0; k < t.length; k++){
                                    imaTermin = false;
                                    let pocetakTermina = parseInt(t[k].pocetak.substring(0,2)) * 60 + parseInt(t[k].pocetak.substring(3,5));
                                    let krajTermina = parseInt(t[k].kraj.substring(0,2)) * 60 + parseInt(t[k].kraj.substring(3,5));
                                    if ((t[k].id == r[j].termin) && (t[k].dan == dan || t[k].datum == datum) && ((vrijeme >= pocetakTermina && vrijeme <= krajTermina))){      
                                        imaTermin = true;
                                        for (let l = 0; l < s.length; l++){
                                            if (s[l].id == r[j].sala){
                                                sala = s[l].naziv;
                                                break;
                                            }
                                        }
                                    }
                                    if (imaTermin) break;
                                }
                            }  
                        }

                        listaOsoblja.push({ime: o[i].ime, prezime: o[i].prezime, sala: sala});
                    }
                    res.send(listaOsoblja);
                }).catch((err) => {console.log("Greška pri dobavljanju sala: " + err);});
            }).catch((err) => {console.log("Greška pri dobavljanju termina: " + err);});
        }).catch((err) => {console.log("Greška pri dobavljanju rezervacija: " + err);});
    }).catch((err) => {console.log("Greška pri dobavljanju osoblja: " + err);});

});

app.listen(8080, () => console.log("App listening on port 8080!"));